use std::thread::sleep;
use std::time::{Duration, SystemTime};
fn main() {
    let now = SystemTime::now();
    loop {
        if let Ok(elapsed) = &now.elapsed() {
            let hours = elapsed.as_secs() / 3600;
            println!(
                "App running for {} hour{}",
                hours,
                if hours != 1 { "s" } else { "" }
            );
        } else {
            println!("App running");
        }
        sleep(Duration::from_secs(3600));
    }
}
